import threading
import time
import Database_mgr

__author__ = 'ubuntu'

import urllib2
import Common
import hashlib
import socket

iplist = ''
request_count = 0


def listAPI():
    URL = Common.Server_ad + "/listAPI"
    MsgBak = urllib2.urlopen(URL).read()
    return MsgBak


'''Run this at least once per 30 seconds In a thread'''


def report(username='dpal887', password='zy19sk30rl2md9', location='1'):
    # IP = Common.myIP
    IP = getNetworkIp()
    port = Common.myPort
    hsh_password = hash256(password)
    URL = Common.Server_ad + "/report?username=" + username + "&password=" + hsh_password + "&ip=" + IP + "&port=" + str(
        port) + "&location=" + location
    try:
        MsgBak = urllib2.urlopen(URL, timeout=20).read()
    except urllib2.URLError, e:
        return "There was an error: %r" % e
    return str(MsgBak) + "  :  " + URL


def hash256(unhsh):
    unhsh += 'COMPSYS302-2015'
    hsh = hashlib.sha256(unhsh).hexdigest()
    return hsh


def logoff(username='dpal887', password='zy19sk30rl2md9'):
    hsh_password = hash256(password)
    URL = Common.Server_ad + "/logoff?username=" + username + "&password=" + hsh_password
    MsgBak = urllib2.urlopen(URL).read()
    return str(MsgBak)


def getList(username='dpal887', password='zy19sk30rl2md9'):
    hsh_password = hash256(password)
    URL = Common.Server_ad + "/getList?username=" + username + "&password=" + hsh_password
    MsgBak = urllib2.urlopen(URL).read()
    return str(MsgBak)


def setGroup(username, password, groupMembers):
    return "setGroup"


def getGroup(username, password, groupID):
    return "getGroup"


def checkin(username, password, location):
    a = True
    while a:
        print 'triggered'
        Database_mgr.clear_list()
        report(username, password, location)
        global iplist
        iplist = getList().replace('0, Online user list returned', '')
        iplist = iplist.split('\r\n')
        list = []
        for i in range(0, len(iplist)):
            list.append(iplist[i].split(','))

        for j in range(1, len(list) - 1):
            print str(list[j])
            Database_mgr.update_ip(list[j][0], list[j][1], list[j][2], list[j][3], list[j][4])

        iplist = str(list)
        time.sleep(30)


def getNetworkIp():
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.connect(('www.google.com', 0))
    return s.getsockname()[0]