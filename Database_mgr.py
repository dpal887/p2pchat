__author__ = 'ubuntu'

import Common
import sqlite3


def setup_database():
    with sqlite3.connect(Common.DB_STRING) as con:
        con.execute("CREATE TABLE user_string (sender, destination, message)")

    with sqlite3.connect(Common.AD_STRING) as con:
        con.execute("CREATE TABLE ip_string (username, location, ip, port, last_login)")


def cleanup_database():
    with sqlite3.connect(Common.DB_STRING) as con:
        con.execute("DROP TABLE user_string")

    with sqlite3.connect(Common.AD_STRING) as con:
        con.execute("DROP TABLE ip_string")


def clear_list():
    with sqlite3.connect(Common.AD_STRING) as con:
        con.execute("DROP TABLE ip_string")
    with sqlite3.connect(Common.AD_STRING) as con:
        con.execute("CREATE TABLE ip_string (username, location, ip, port, last_login)")


def update_table(sender, destination, message):
    message = Common.clean(message)
    with sqlite3.connect(Common.DB_STRING) as c:
        c.execute("INSERT INTO user_string VALUES (?, ?, ?)",
                  [sender, destination, message])
    c.close()


def update_ip(username, location, ip, port, last_login):
    with sqlite3.connect(Common.AD_STRING) as c:
        c.execute("INSERT INTO ip_string VALUES (?, ?, ?, ?, ?)",
                  [username, location, ip, port, last_login])
    c.close()


def pull_message(search_term):
    with sqlite3.connect(Common.DB_STRING) as c:
        output = c.execute("SELECT message FROM user_string")
    return output.fetchall()


def pull_user(search_term):
    with sqlite3.connect(Common.DB_STRING) as c:
        output = c.execute("SELECT sender FROM user_string")
    return output.fetchall()


def pull_peer_user(search_term):
    with sqlite3.connect(Common.AD_STRING) as c:
        output = c.execute("SELECT username FROM ip_string")
    return output.fetchall()

def pull_peer_user_srch(search_term):
    with sqlite3.connect(Common.AD_STRING) as c:
        output = c.execute("SELECT username FROM ip_string WHERE ip=? ", [search_term,])
    return output.fetchall()


def pull_peer_location(search_term):
    with sqlite3.connect(Common.AD_STRING) as c:
        output = c.execute("SELECT location FROM ip_string")
    return output.fetchall()


def pull_peer_ip(search_term):
    with sqlite3.connect(Common.AD_STRING) as c:
        output = c.execute("SELECT ip FROM ip_string")
    return output.fetchall()


def pull_peer_port(search_term):
    with sqlite3.connect(Common.AD_STRING) as c:
        output = c.execute("SELECT port FROM ip_string")
    return output.fetchall()


def pull_peer_ll(search_term):
    with sqlite3.connect(Common.AD_STRING) as c:
        output = c.execute("SELECT last_login FROM ip_string")
    return output.fetchall()
