import thread
import time

__author__ = 'ubuntu'

import cherrypy
import Common
import Database_mgr
import os
import Version1
import ServerSide
import json

_cp_config = {'tools.encode.on': True,
              'tools.encode.encoding': 'utf-8',
              'tools.sessions.on': 'True',
              }

peer_ip = '0.0.0.0'

class Main(object):
    @cherrypy.expose()
    def index(self):
        page = "<html>"
        page += '<head>'
        page += '</head>'
        page += '<body>'
        page += '<h1>WELCOME</h1>'
        page += '<form action="/login">'
        page += '<input type="submit" value="Login">'
        page += '</form>'
        page += '</body>'
        page += "</html>"
        return page

    @cherrypy.expose
    def login(self, error=0):
        Page = ''
        if error == 1:
            Page += '<font color=red><h1>Error, Incorrect Username or Password</h1></font>'
        Page += '<form action="/signin" method="post" enctype="multipart/form-data">'
        Page += 'Username: <input type="text" name="username"/><br/>'
        Page += 'Password: <input type="password" name="password"/>'
        Page += '<input type="submit" value="Login"/></form>'
        return Page

    @cherrypy.expose()
    def signin(self, username=None, password=None):
        username = Common.clean(username)
        if self.authenitcate(username, password) == 0:
            cherrypy.session['username'] = username
            cherrypy.session['password'] = password
            thread.start_new_thread(Version1.checkin,
                                    (cherrypy.session['username'], cherrypy.session['password'], Common.location))
            raise cherrypy.HTTPRedirect('/main')
        else:
            raise cherrypy.HTTPRedirect('/login')

    @cherrypy.expose()
    def authenitcate(self, username=None, password=None):
        code = Version1.report(username, password, Common.location)
        return int(code[0])

    @cherrypy.expose()
    def main(self, response='0'):
        page = "<html>"
        page += '<head>'
        page += '<link href="/static/css/style.css" rel="stylesheet">'
        page += '</head>'
        page += '<body>'
        page += '<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>'
        page += '<script>'
        page += 'function autoRefresh_div(){'
        page += '$("#div2").load("test");'
        page += '$("#div3").load("testtwo");}'
        page += "setInterval('autoRefresh_div()', 50);"
        page += '</script>'
        page += '<div id=div1><h1>'
        try:
            page += 'Your session name is: ' + cherrypy.session['username']
        except KeyError:
            page += '<a href=/login>Please Login</a>'
            raise cherrypy.HTTPRedirect('/login')
        page += '</h1><table bgcolor="#00FF00"><tr><td bgcolor=red width="500px">'
        page += '<form action="/post" method="post" enctype="multipart/form-data">'
        page += 'Message.......: <input type="text" name="message"/><br>'
        page += 'Manual Send: <input type="text" value=' + peer_ip + ' name="ip"/>'
        page += '<input type="submit" value="Send"/></form>'
        page += '</td><td><h1>IP LIST</h1></td></tr><tr><td valign=top>'
        if response == '1':
            page += '<h2><font color=green>Success</font></h2>'
        if response == '2':
            page += '<h2><font color=red>Failed</font></h2>'
        page += '<div id="div2">'
        page += '</div>'
        page += '</td><td valign=top>'
        page += '</div><div id="div3">'
        page += '</div>'
        page += '</div>'
        page += '<form action="/log_off">'
        page += '<input type="submit" value="Log Off">'
        page += '</form>'
        page += '</tr>'
        page += '</body>'
        page += '</html>'
        return page

    @cherrypy.expose()
    def test(self):
        page = ''
        try:
            db_mesg = Database_mgr.pull_message(cherrypy.session['username'])
            db_user = Database_mgr.pull_user(cherrypy.session['username'])
            for i in range(len(db_mesg) - 1, -1, -1):
                page += "<br><b>" + unicode(db_user[i][0]) + ": </b>"
                page += unicode(db_mesg[i][0]) + "</br>"
            return page
        except KeyError:
            page += 'Please Login'
        return page


    @cherrypy.expose()
    def testtwo(self):
        page = ''
        for i in range(len(Database_mgr.pull_peer_ip(None))):
            page += '<br>User: ' + Database_mgr.pull_peer_user(None)[i][0] + '</br>'
            page += '<br>IP: ' + Database_mgr.pull_peer_ip(None)[i][0] + '</br>'
            page += '<br>Port: ' + Database_mgr.pull_peer_port(None)[i][0] + '</br>'
            page += '<br>Location: ' + Database_mgr.pull_peer_location(None)[i][0] + '</br>'
            page += '<form action="/set_sender_ip">'
            page += '<input type="submit" name="ip" value="' + Database_mgr.pull_peer_ip(None)[i][0] + ':' + Database_mgr.pull_peer_port(None)[i][0] + '">'
            page += '</form>'
        return page

    @cherrypy.expose()
    def set_sender_ip(self, ip='0.0.0.0'):
        global peer_ip
        peer_ip = ip
        raise cherrypy.HTTPRedirect('/main')

    @cherrypy.tools.json_out()
    @cherrypy.expose()
    def post(self, message, ip=peer_ip):
        error = ServerSide.send_message(ip, message, cherrypy.session['username'], Database_mgr.pull_peer_user_srch(ip.split(':')[0])[0][0])
        if error != '1':
            raise cherrypy.HTTPRedirect('/main?response=2')
        elif error == '1':
            Database_mgr.update_table(cherrypy.session['username'], cherrypy.session['username'], message)
            raise cherrypy.HTTPRedirect('/main?response=1')

    @cherrypy.expose()
    def error(self, err=None):
        page = '<font color=red><h1>' + err + '</h1></font>'
        page += '<form action="/main">'
        page += '<input type="submit" value="Back">'
        page += '</form>'
        return page

    @cherrypy.expose()
    def default(self, *args, **kwargs):
        return "<h1>ERROR: 404 Not found</h1>"

    @cherrypy.expose()
    def apilist(self):
        Page = "<html>"
        Page += "<head>"
        Page += "</head>"
        Page += "<body>"
        iteminput = Version1.listAPI()
        output = iteminput.split('/')
        for i in range(0, len(output)):
            Page += "<br>" + unicode(output[i]) + "</br>"
        Page += "</body>"
        Page += "</html>"
        return Page

    @cherrypy.expose()
    def report(self):
        code = int(Version1.report().split(',')[0])
        if code == 0:
            raise cherrypy.HTTPRedirect('/main')
        else:
            Page = "Supreme Leader Says no"
        return Page

    @cherrypy.expose()
    def log_off(self):
        code = int(Version1.logoff().split(',')[0])
        if code == 0:
            Page = "You Logged Off"
        else:
            Page = "Error Logging off"
        return Page

    @cherrypy.tools.json_in()
    @cherrypy.expose()
    def receiveMessage(self):
        data = cherrypy.request.json
        jsonFormat = unicode(type(data))
        message = Common.clean(data['message'])
        sender = Common.clean(data['sender'])
        recipiant = Common.clean(data['destination'])
        Database_mgr.update_table(sender, recipiant, "["+jsonFormat+"]"+message)
        return str(data)


if __name__ == '__main__':
    conf = {
        '/': {
            'tools.sessions.on': True,
            'tools.staticdir.root': os.path.abspath(os.getcwd())
        },
        '/static': {
            'tools.staticdir.on': True,
            'tools.staticdir.dir': './public'
        }
    }
    cherrypy.tree.mount(Main(), '/', conf)

    cherrypy.config.update({'server.socket_host': Common.listen_ip,
                            'server.socket_port': Common.listen_port,
                            'engine.autoreload.on': True,
                            })
    cherrypy.engine.subscribe('start', Database_mgr.setup_database)
    cherrypy.engine.subscribe('stop', Database_mgr.cleanup_database)
    cherrypy.engine.start()

    cherrypy.engine.block()