__author__ = 'ubuntu'

import sqlite3


DB_STRING = "my.sqlite"
AD_STRING = "ad.sqlite"
def setup_database():
    """
    Create the `user_string` table in the database
    on server startup
    """
    with sqlite3.connect(DB_STRING) as con:
        con.execute("CREATE TABLE user_string (session_id, value)")
    with sqlite3.connect(AD_STRING) as con:
        con.execute("CREATE TABLE addresses (session_id, value)")


def cleanup_database():
    """
    Destroy the `user_string` table from the database
    on server shutdown.
    """
    with sqlite3.connect(DB_STRING) as con:
        con.execute("DROP TABLE user_string")

    with sqlite3.connect(AD_STRING) as con:
        con.execute("DROP TABLE addresses")