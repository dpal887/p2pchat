import os
import sqlite3

__author__ = 'ubuntu'


import cherrypy
import urllib2

DB_STRING = "my.sqlite"
AD_STRING = "ad.sqlite"

class Main(object):

    @cherrypy.expose()
    def index(self):
        Page =  "<html>"
        Page += "<head></head>"
        Page += "<body>"
        Page += "<br><h1>Welcome to C302 Protocol Alpha</h1><br/>"
        try:
            Page += "Hello " + cherrypy.session['username'] + "!"
            Page += "<br>Enter Message To Send</br>"
            Page += '<form action="/send_message" method="post" enctype="multipart/form-data">'
            Page += 'Message: <input type="text" value="no_message" name="message"/><br/>'
            Page += 'IP     : <input type="text" value="0.0.0.0" name="IP"/>'
            Page += 'PORT   : <input type="text" value="10003" name="port"/>'
            Page += '<input type="submit" value="Send"/></form>'
            with sqlite3.connect("my.sqlite") as c:
                output = c.execute("SELECT value FROM user_string",)
            try:
                Incoming = output.fetchall()
                for i in range(0,len(Incoming)):
                    Page += "<br>" + unicode(Incoming[i][0]) + "</br>"
            except KeyError:
                Page += "Fatal Error. The recieved Message broke the engine"
        except KeyError:
            Page += "<a><h2>Click here to <a href='login'>login</h2></a>."
        Page += "<body>"
        Page += "</html>"
        return Page

    @cherrypy.expose()
    def login(self):
        Page =  "<html>"
        Page += "<head></head>"
        Page += "<body>"
        Page += "<br><h1>Login Page</h1><br/>"
        Page += '<form action="/signin" method="post" enctype="multipart/form-data">'
        Page += 'Username: <input type="text" name="username"/><br/>'
        Page += 'Password: <input type="text" name="password"/>'
        Page += '<input type="submit" value="Login"/></form>'
        Page += "<body>"
        Page += "</html>"
        return Page

    @cherrypy.expose
    def signin(self, username="Daniel", password=None):
        """Check their name and password and send them either to the main page, or back to the main login screen."""
        error = self.authoriseUserLogin(username,password)
        if (error == 0):
            cherrypy.session['username'] = username
            raise cherrypy.HTTPRedirect('/')
        else:
            raise cherrypy.HTTPRedirect('/login')
    #TODO: ADD AUTHENTICATION
    def authoriseUserLogin(self, username, password):
        return 0

    @cherrypy.expose()
    def send_message(self,IP='0.0.0.0', port="10002", message='failure'):
        URL = "http://"+IP+":"+port+"/receiveMessage?message="+cherrypy.session['username']+": "+message+"&IP=0.0.0.0&port=10002"
        URL = URL.replace(' ', '+')
        urllib2.urlopen(URL)
        with sqlite3.connect("my.sqlite") as c:
            c.execute("INSERT INTO user_string VALUES (?, ?)",
                      [IP+":"+port, message])
        raise cherrypy.HTTPRedirect('/')

    @cherrypy.expose()
    def receiveMessage(self, IP, port, message):
        with sqlite3.connect("my.sqlite") as c:
            c.execute("INSERT INTO user_string VALUES (?, ?)",
                      [IP+":"+port, message])
        return "Recieved"

def setup_database():
    """
    Create the `user_string` table in the database
    on server startup
    """
    with sqlite3.connect(DB_STRING) as con:
        con.execute("CREATE TABLE user_string (session_id, value)")
    with sqlite3.connect(AD_STRING) as con:
        con.execute("CREATE TABLE addresses (session_id, value)")


def cleanup_database():
    """
    Destroy the `user_string` table from the database
    on server shutdown.
    """
    with sqlite3.connect(DB_STRING) as con:
        con.execute("DROP TABLE user_string")

    with sqlite3.connect(AD_STRING) as con:
        con.execute("DROP TABLE addresses")


if __name__ == '__main__':
    conf = {
        '/': {
            'tools.sessions.on': True,
            'tools.staticdir.root': os.path.abspath(os.getcwd())
        },
        '/static': {
            'tools.staticdir.on': True,
            'tools.staticdir.dir': './public'
        }
    }
    cherrypy.config.update({'server.socket_host': '0.0.0.0',
                        'server.socket_port': 10002,
                        'engine.autoreload.on': True,
                        })
    cherrypy.engine.subscribe('start', setup_database)
    cherrypy.engine.subscribe('stop', cleanup_database)
    cherrypy.quickstart(Main(), '/', conf)
    cherrypy.engine.block()