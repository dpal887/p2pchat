import os
import os.path
import cherrypy
import sqlite3

DB_STRING = "my.sqlite"
AD_STRING = "ad.sqlite"

cherrypy.config.update({'server.socket_host': '0.0.0.0',
                        'server.socket_port': 10002,
                        'engine.autoreload.on': True,
                        })


class Main(object):
    _cp_config = {'tools.encode.on': True,
                  'tools.encode.encoding': 'utf-8',
                  'tools.sessions.on': 'True',
                  }

    @cherrypy.expose()
    def index(self):
        Page = "Welcome<br/>"
        Page += """<link href="/static/css/style.css" rel="stylesheet">"""

        try:
            Page += "Hello " + cherrypy.session['username'] + "!"
            Page += "<a href='readfromfile'>Begin</a>"
            self.add_ad()
        except KeyError:  # There is no username
            Page += """<div id="rectangle"></div>"""
            Page += "Click here to <a href='login'>login</a>."
        return Page

    @cherrypy.expose
    def login(self):
        Page = '<form action="/signin" method="post" enctype="multipart/form-data">'
        Page += 'Username: <input type="text" name="username"/><br/>'
        Page += 'Password: <input type="text" name="password"/>'
        Page += '<input type="submit" value="Login"/></form>'
        return Page

    @cherrypy.expose()
    def signin(self, username=None, password=None):
        error = self.authoriseUserLogin(username, password)
        if error == 0:
            cherrypy.session['username'] = username
            raise cherrypy.HTTPRedirect('/')
        else:
            raise cherrypy.HTTPRedirect('/login')

    def authoriseUserLogin(self, username, password):
        print username
        print password
        return 0

    @cherrypy.expose()
    def writetofile(self, message=""):
        f = open('message.txt', 'a')
        try:
            message = message.replace('<', '(')
            message = message.replace('>', ')')
            f.write("\n" + cherrypy.session['username'] + ": " + message)
        except KeyError:  # There is no username
            return "Click here to <a href='login'>login</a>."
        f = open('message.txt', 'r')
        h = f.readlines()
        historylist = ""
        for i in range(len(h) - 1, 0, -1):
            historylist += """<br>"""
            historylist += h[i]
            historylist += """</br>"""

        with sqlite3.connect(DB_STRING) as c:
            c.execute("INSERT INTO user_string VALUES (?, ?)",
                      [cherrypy.session['username'], message])

        f.close()
        ip = open('Online.txt', 'r')
        q = ip.readlines()
        newlist = ""

        for j in range(len(q) - 1, 0, -1):
            newlist += """<br>"""
            newlist += q[j]
            newlist += """</br>"""

        return """<html>
          <head>
          <link href="/static/css/style.css" rel="stylesheet">
          </head>
          <body>

          <table>
          <tr>
            <td width="200px">
                <form method="get" action="writetofile">
                <input type="text" value="" name="message" />
                <button type="submit" class='oioi'>Send</button>
                </form>
                <form method="post" action="readfromfile">
                <button type="submit" class='oioi'>Receive</button>
                <h1><font color="red">MESSAGES</font></h1>
                <br><font color="red">""" + historylist + """</font></br>
                </form>
            </td>
            <td valign="top">
                <h1><font color="green">ONLINE</font></h1>
                <font color="green">""" + newlist + """</font>
            </td>
          </tr>

          </table>

          </body>
        </html>"""

    @cherrypy.expose()
    def readfromfile(self, input=0):
        f = open('message.txt', 'r')
        h = f.readlines()
        try:
            cherrypy.session['username']
        except KeyError:  # There is no username
            return "Click here to <a href='login'>login</a>."
        historylist = ""

        for i in range(len(h) - 1, 0, -1):
            historylist += """<br>"""
            historylist += h[i]
            historylist += """</br>"""

        f.close()
        ip = open('Online.txt', 'r')
        q = ip.readlines()
        newlist = ""

        for j in range(len(q) - 1, 0, -1):
            newlist += """<br>"""
            newlist += q[j]
            newlist += """</br>"""

        return """<html>
          <head>
          <link href="/static/css/style.css" rel="stylesheet">
          </head>
          <body>

          <table>
          <tr>
            <td width="200px">
                <form method="get" action="writetofile">
                <input type="text" value="" name="message" />
                <button type="submit" class='oioi'>Send</button>
                </form>
                <form method="post" action="readfromfile">
                <button type="submit" class='oioi'>Receive</button>
                <h1><font color="red">MESSAGES</font></h1>
                <br><font color="red">""" + historylist + """</font></br>
                </form>
            </td>
            <td valign="top">
                <h1><font color="green">ONLINE</font></h1>
                <font color="green">""" + newlist + """</font>
            </td>
          </tr>

          </table>

          </body>
        </html>"""

    @cherrypy.expose()
    @cherrypy.tools.accept(media='text/plain')
    def externalget(self):
        with sqlite3.connect(DB_STRING) as c:
            output = c.execute("SELECT value FROM user_string WHERE session_id=?",
                               [cherrypy.session['username']])
            return output.fetchone()

    def add_ad(self):
        from urllib2 import urlopen
        my_ip = urlopen('http://ip.42.pl/raw').read()
        with sqlite3.connect(AD_STRING) as c:
            c.execute("INSERT INTO addresses VALUES (?, ?)",
                      [cherrypy.session['username'], my_ip])
        return 0


def setup_database():
    """
    Create the `user_string` table in the database
    on server startup
    """
    with sqlite3.connect(DB_STRING) as con:
        con.execute("CREATE TABLE user_string (session_id, value)")
    with sqlite3.connect(AD_STRING) as con:
        con.execute("CREATE TABLE addresses (session_id, value)")


def cleanup_database():
    """
    Destroy the `user_string` table from the database
    on server shutdown.
    """
    with sqlite3.connect(DB_STRING) as con:
        con.execute("DROP TABLE user_string")

    with sqlite3.connect(AD_STRING) as con:
        con.execute("DROP TABLE addresses")


if __name__ == '__main__':
    conf = {
        '/': {
            'tools.sessions.on': True,
            'tools.staticdir.root': os.path.abspath(os.getcwd())
        },
        '/static': {
            'tools.staticdir.on': True,
            'tools.staticdir.dir': './public'
        }
    }
    cherrypy.engine.subscribe('start', setup_database)
    cherrypy.engine.subscribe('stop', cleanup_database)
    cherrypy.quickstart(Main(), '/', conf)
    cherrypy.engine.block()