__author__ = 'ubuntu'

import os
import os.path
import sqlite3
import data_handler
import socket

import cherrypy


class Main(object):

    @cherrypy.expose()
    def index(self):

        page = ""
        page += "<h1>Welcome</h1>"
        try:
            page += cherrypy.session['username']
            raise cherrypy.HTTPRedirect('/post')
        except KeyError:
            page += "</br>"
            page += "<h3>You are required to log in</h3>"
            raise cherrypy.HTTPRedirect('/post')

    @cherrypy.expose()
    def receiveMessage(self, IP = '0.0.0.0', port = '10002/404', message = 'Jo',):
        with sqlite3.connect("my.sqlite") as c:
            c.execute("INSERT INTO user_string VALUES (?, ?)",
                      [message, message])
        raise cherrypy.HTTPRedirect("http://"+IP+":"+port)

    @cherrypy.expose()
    def post(self):
        peerip = "172.23.88.175"
        peerport = "1234"
        page = '<form action="http://'+peerip+":"+peerport+"/receiveMessage"+'" method="post" enctype="multipart/form-data">'
        page += 'IP: <input type="text" value="0.0.0.0" name="IP"/><br/>'
        page += 'port: <input type="text" value="10002" name="port"/>'
        page += 'message: <input type="text" name="message"/>'
        page += '<input type="submit" value="Login"/></form>'
        return page

    @cherrypy.expose()
    def receive(self):
        with sqlite3.connect("my.sqlite") as c:
            output = c.execute("SELECT value FROM user_string",)
            page = "<html>"
            page +="<head>"
            page +='<meta http-equiv="refresh" content="5" />'
            page +="<head>"
            page +="body"
            page += output.fetchone()
            page +="body"
            page +="/html"
            return page

    cherrypy.engine.subscribe('start', data_handler.setup_database)
    cherrypy.engine.subscribe('stop', data_handler.cleanup_database)
if __name__ == '__main__':
    conf = {
        '/': {
            'tools.sessions.on': True,
            'tools.staticdir.root': os.path.abspath(os.getcwd()),
        },
        '/static': {
            'tools.staticdir.on': True,
            'tools.staticdir.dir': './public'
        }
    }
    cherrypy.tree.mount(Main(), '/', config=conf)

    cherrypy.config.update({'server.socket_host': '0.0.0.0',
                        'server.socket_port': 10002,
                        'engine.autoreload.on': True,
                        })

    cherrypy.engine.start()
    cherrypy.engine.block()


